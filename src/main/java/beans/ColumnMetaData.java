package beans;

import java.sql.Date;

public class ColumnMetaData {

	private String col_id;
	private String env_id;
	private String col_name;
	private String col_desc;
	private String col_data_type;
	private int col_length;
	private boolean is_date;
	private boolean is_amount;
	private boolean is_calculated;
	private String calc_id;
	private String limits_Count;
	private String col_domain;
	private boolean active;
	private boolean is_Nullable;
	
	
	private Date eff_dt;
	private String updated_by;
	
	
	public String getCol_id() {
		return col_id;
	}
	public void setCol_id(String col_id) {
		this.col_id = col_id;
	}
	public String getEnv_id() {
		return env_id;
	}
	public void setEnv_id(String env_id) {
		this.env_id = env_id;
	}
	public String getCol_name() {
		return col_name;
	}
	public void setCol_name(String col_name) {
		this.col_name = col_name;
	}
	public String getCol_desc() {
		return col_desc;
	}
	public void setCol_desc(String col_desc) {
		this.col_desc = col_desc;
	}
	public String getCol_data_type() {
		return col_data_type;
	}
	public void setCol_data_type(String col_data_type) {
		this.col_data_type = col_data_type;
	}
	public int getCol_length() {
		return col_length;
	}
	public void setCol_length(int col_length) {
		this.col_length = col_length;
	}
	public boolean isIs_date() {
		return is_date;
	}
	public void setIs_date(boolean is_date) {
		this.is_date = is_date;
	}
	public boolean isIs_amount() {
		return is_amount;
	}
	public void setIs_amount(boolean is_amount) {
		this.is_amount = is_amount;
	}
	public boolean isIs_calculated() {
		return is_calculated;
	}
	public void setIs_calculated(boolean is_calculated) {
		this.is_calculated = is_calculated;
	}
	public String getCalc_id() {
		return calc_id;
	}
	public void setCalc_id(String calc_id) {
		this.calc_id = calc_id;
	}
	
	public String getCol_domain() {
		return col_domain;
	}
	public void setCol_domain(String col_domain) {
		this.col_domain = col_domain;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getLimits_Count() {
		return limits_Count;
	}
	public void setLimits_Count(String limits_Count) {
		this.limits_Count = limits_Count;
	}
	public boolean isIs_Nullable() {
		return is_Nullable;
	}
	public void setIs_Nullable(boolean is_Nullable) {
		this.is_Nullable = is_Nullable;
	}
	public Date getEff_dt() {
		return eff_dt;
	}
	public void setEff_dt(Date eff_dt) {
		this.eff_dt = eff_dt;
	}
	public String getUpdated_by() {
		return updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}
	
	
}
