package beans;

public class CalcColumnBean {

	private String dateCol;
	private String amtCol;
	public String getDateCol() {
		return dateCol;
	}
	public void setDateCol(String dateCol) {
		this.dateCol = dateCol;
	}
	public String getAmtCol() {
		return amtCol;
	}
	public void setAmtCol(String amtCol) {
		this.amtCol = amtCol;
	}
}
