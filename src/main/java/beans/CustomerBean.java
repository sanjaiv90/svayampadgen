package beans;

import java.sql.Date;

public class CustomerBean {
	
private Integer cust_id;
private String cust_local_src_sys_cd;
private String cust_local_num;
private String cust_suff_num ;
private Date cust_eff_dt;
private Date cust_wrk_dt;
private Date cust_proc_dt;
private String cust_status_cd;
private String cust_ext_cd;
private String cust_orig_chnl_cd;
private String cust_det_prod_cd;
private String cust_prim_cust_fname;
private String cust_prim_cust_lname;
private String cust_prim_cust_id;
private String cust_prim_cust_addr1;
private String cust_prim_cust_addr2;
private String cust_prim_cust_addr_city;
private String cust_prim_cust_addr_state;
private String cust_prim_cust_addr_ctry;
private String cust_prim_cust_addr_zip;

public Integer getCust_id() {
	return cust_id;
}
public void setCust_id(Integer cust_id) {
	this.cust_id = cust_id;
}
public String getCust_local_src_sys_cd() {
	return cust_local_src_sys_cd;
}
public void setCust_local_src_sys_cd(String cust_local_src_sys_cd) {
	this.cust_local_src_sys_cd = cust_local_src_sys_cd;
}
public String getCust_local_num() {
	return cust_local_num;
}
public void setCust_local_num(String cust_local_num) {
	this.cust_local_num = cust_local_num;
}
public String getCust_suff_num() {
	return cust_suff_num;
}
public void setCust_suff_num(String cust_suff_num) {
	this.cust_suff_num = cust_suff_num;
}
public Date getCust_eff_dt() {
	return cust_eff_dt;
}
public void setCust_eff_dt(Date cust_eff_dt) {
	this.cust_eff_dt = cust_eff_dt;
}
public Date getCust_wrk_dt() {
	return cust_wrk_dt;
}
public void setCust_wrk_dt(Date cust_wrk_dt) {
	this.cust_wrk_dt = cust_wrk_dt;
}
public Date getCust_proc_dt() {
	return cust_proc_dt;
}
public void setCust_proc_dt(Date cust_proc_dt) {
	this.cust_proc_dt = cust_proc_dt;
}
public String getCust_status_cd() {
	return cust_status_cd;
}
public void setCust_status_cd(String cust_status_cd) {
	this.cust_status_cd = cust_status_cd;
}
public String getCust_ext_cd() {
	return cust_ext_cd;
}
public void setCust_ext_cd(String cust_ext_cd) {
	this.cust_ext_cd = cust_ext_cd;
}
public String getCust_orig_chnl_cd() {
	return cust_orig_chnl_cd;
}
public void setCust_orig_chnl_cd(String cust_orig_chnl_cd) {
	this.cust_orig_chnl_cd = cust_orig_chnl_cd;
}
public String getCust_det_prod_cd() {
	return cust_det_prod_cd;
}
public void setCust_det_prod_cd(String cust_det_prod_cd) {
	this.cust_det_prod_cd = cust_det_prod_cd;
}
public String getCust_prim_cust_fname() {
	return cust_prim_cust_fname;
}
public void setCust_prim_cust_fname(String cust_prim_cust_fname) {
	this.cust_prim_cust_fname = cust_prim_cust_fname;
}
public String getCust_prim_cust_lname() {
	return cust_prim_cust_lname;
}
public void setCust_prim_cust_lname(String cust_prim_cust_lname) {
	this.cust_prim_cust_lname = cust_prim_cust_lname;
}
public String getCust_prim_cust_id() {
	return cust_prim_cust_id;
}
public void setCust_prim_cust_id(String cust_prim_cust_id) {
	this.cust_prim_cust_id = cust_prim_cust_id;
}
public String getCust_prim_cust_addr1() {
	return cust_prim_cust_addr1;
}
public void setCust_prim_cust_addr1(String cust_prim_cust_addr1) {
	this.cust_prim_cust_addr1 = cust_prim_cust_addr1;
}
public String getCust_prim_cust_addr2() {
	return cust_prim_cust_addr2;
}
public void setCust_prim_cust_addr2(String cust_prim_cust_addr2) {
	this.cust_prim_cust_addr2 = cust_prim_cust_addr2;
}
public String getCust_prim_cust_addr_city() {
	return cust_prim_cust_addr_city;
}
public void setCust_prim_cust_addr_city(String cust_prim_cust_addr_city) {
	this.cust_prim_cust_addr_city = cust_prim_cust_addr_city;
}
public String getCust_prim_cust_addr_state() {
	return cust_prim_cust_addr_state;
}
public void setCust_prim_cust_addr_state(String cust_prim_cust_addr_state) {
	this.cust_prim_cust_addr_state = cust_prim_cust_addr_state;
}
public String getCust_prim_cust_addr_ctry() {
	return cust_prim_cust_addr_ctry;
}
public void setCust_prim_cust_addr_ctry(String cust_prim_cust_addr_ctry) {
	this.cust_prim_cust_addr_ctry = cust_prim_cust_addr_ctry;
}
public String getCust_prim_cust_addr_zip() {
	return cust_prim_cust_addr_zip;
}
public void setCust_prim_cust_addr_zip(String cust_prim_cust_addr_zip) {
	this.cust_prim_cust_addr_zip = cust_prim_cust_addr_zip;
}
}
