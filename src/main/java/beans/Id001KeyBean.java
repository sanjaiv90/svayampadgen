 package beans;

import java.math.BigDecimal;
import java.sql.Date;

import com.sun.org.apache.xpath.internal.operations.Equals;

public class Id001KeyBean {
	private long pad_key_id;
	private long cust_id;
	private String cust_local_srs_sys_cd;
	private String cust_local_num;
	private String cust_suff_num ;
	private Date cust_eff_dt;
	private Date cust_wrk_dt;
	private Date cust_proc_dt;
	private String cust_status_cd;
	private String cust_ext_cd;
	private String cust_orig_chnl_cd;
	private String cust_det_prod_cd;
	private String cust_prim_cust_fname;
	private String cust_prim_cust_lname;
	private String cust_prim_cust_id;
	private String cust_prim_cust_addr1;
	private String cust_prim_cust_addr2;
	private String cust_prim_cust_addr_city;
	private String cust_prim_cust_addr_state;
	private String cust_prim_cust_addr_ctry;
	private String cust_prim_cust_addr_zip;
	private String txn_desc;
	private String  txn_typ_cd;
	private String  txn_cust_ind;
	private String  rpt_unit_num;
	private String  cct_num;
	private String  src_chart_fld;
	private String  eng_src_cd ;
	private String  ent_mthd_cd;
	private String  prod_chart_fld;
	private String  det_prod_cd;
	private String  cust_typ_chtfld ;
	private String  det_cust_type_cd ;
	private String  cust_portfolio_cd ;
	private String  book_cd_chart_fld ;
	private String  ledger_cd;
	private String  acct_chart_fld;
	private String  chnl_cd ;
	private String  src_curr_cd;
	private String  src_curr_typ_cd;
	private String  tgt_curr_cd ;
	private String  tgt_curr_typ_cd ;
	private String  cr_db_ind;
	public long getPad_key_id() {
		return pad_key_id;
	}
	public void setPad_key_id(long pad_key_id) {
		this.pad_key_id = pad_key_id;
	}
	public long getCust_id() {
		return cust_id;
	}
	public void setCust_id(long cust_id) {
		this.cust_id = cust_id;
	}
	public String getCust_local_src_sys_cd() {
		return cust_local_srs_sys_cd;
	}
	public void setCust_local_src_sys_cd(String cust_local_srs_sys_cd) {
		this.cust_local_srs_sys_cd = cust_local_srs_sys_cd;
	}
	public String getCust_local_num() {
		return cust_local_num;
	}
	public void setCust_local_num(String cust_local_num) {
		this.cust_local_num = cust_local_num;
	}
	public String getCust_suff_num() {
		return cust_suff_num;
	}
	public void setCust_suff_num(String cust_suff_num) {
		this.cust_suff_num = cust_suff_num;
	}
	public Date getCust_eff_dt() {
		return cust_eff_dt;
	}
	public void setCust_eff_dt(Date cust_eff_dt) {
		this.cust_eff_dt = cust_eff_dt;
	}
	public Date getCust_wrk_dt() {
		return cust_wrk_dt;
	}
	public void setCust_wrk_dt(Date cust_wrk_dt) {
		this.cust_wrk_dt = cust_wrk_dt;
	}
	public Date getCust_proc_dt() {
		return cust_proc_dt;
	}
	public void setCust_proc_dt(Date cust_proc_dt) {
		this.cust_proc_dt = cust_proc_dt;
	}
	public String getCust_status_cd() {
		return cust_status_cd;
	}
	public void setCust_status_cd(String cust_status_cd) {
		this.cust_status_cd = cust_status_cd;
	}
	public String getCust_ext_cd() {
		return cust_ext_cd;
	}
	public void setCust_ext_cd(String cust_ext_cd) {
		this.cust_ext_cd = cust_ext_cd;
	}
	public String getCust_orig_chnl_cd() {
		return cust_orig_chnl_cd;
	}
	public void setCust_orig_chnl_cd(String cust_orig_chnl_cd) {
		this.cust_orig_chnl_cd = cust_orig_chnl_cd;
	}
	public String getCust_det_prod_cd() {
		return cust_det_prod_cd;
	}
	public void setCust_det_prod_cd(String cust_det_prod_cd) {
		this.cust_det_prod_cd = cust_det_prod_cd;
	}
	public String getCust_prim_cust_fname() {
		return cust_prim_cust_fname;
	}
	public void setCust_prim_cust_fname(String cust_prim_cust_fname) {
		this.cust_prim_cust_fname = cust_prim_cust_fname;
	}
	public String getCust_prim_cust_lname() {
		return cust_prim_cust_lname;
	}
	public void setCust_prim_cust_lname(String cust_prim_cust_lname) {
		this.cust_prim_cust_lname = cust_prim_cust_lname;
	}
	public String getCust_prim_cust_id() {
		return cust_prim_cust_id;
	}
	public void setCust_prim_cust_id(String cust_prim_cust_id) {
		this.cust_prim_cust_id = cust_prim_cust_id;
	}
	public String getCust_prim_cust_addr1() {
		return cust_prim_cust_addr1;
	}
	public void setCust_prim_cust_addr1(String cust_prim_cust_addr1) {
		this.cust_prim_cust_addr1 = cust_prim_cust_addr1;
	}
	public String getCust_prim_cust_addr2() {
		return cust_prim_cust_addr2;
	}
	public void setCust_prim_cust_addr2(String cust_prim_cust_addr2) {
		this.cust_prim_cust_addr2 = cust_prim_cust_addr2;
	}
	public String getCust_prim_cust_addr_city() {
		return cust_prim_cust_addr_city;
	}
	public void setCust_prim_cust_addr_city(String cust_prim_cust_addr_city) {
		this.cust_prim_cust_addr_city = cust_prim_cust_addr_city;
	}
	public String getCust_prim_cust_addr_state() {
		return cust_prim_cust_addr_state;
	}
	public void setCust_prim_cust_addr_state(String cust_prim_cust_addr_state) {
		this.cust_prim_cust_addr_state = cust_prim_cust_addr_state;
	}
	public String getCust_prim_cust_addr_ctry() {
		return cust_prim_cust_addr_ctry;
	}
	public void setCust_prim_cust_addr_ctry(String cust_prim_cust_addr_ctry) {
		this.cust_prim_cust_addr_ctry = cust_prim_cust_addr_ctry;
	}
	public String getCust_prim_cust_addr_zip() {
		return cust_prim_cust_addr_zip;
	}
	public void setCust_prim_cust_addr_zip(String cust_prim_cust_addr_zip) {
		this.cust_prim_cust_addr_zip = cust_prim_cust_addr_zip;
	}
	public String getTxn_desc() {
		return txn_desc;
	}
	public void setTxn_desc(String txn_desc) {
		this.txn_desc = txn_desc;
	}
	public String getTxn_typ_cd() {
		return txn_typ_cd;
	}
	public void setTxn_typ_cd(String txn_typ_cd) {
		this.txn_typ_cd = txn_typ_cd;
	}
	public String getTxn_cust_ind() {
		return txn_cust_ind;
	}
	public void setTxn_cust_ind(String txn_cust_ind) {
		this.txn_cust_ind = txn_cust_ind;
	}
	public String getRpt_unit_num() {
		return rpt_unit_num;
	}
	public void setRpt_unit_num(String rpt_unit_num) {
		this.rpt_unit_num = rpt_unit_num;
	}
	public String getCct_num() {
		return cct_num;
	}
	public void setCct_num(String cct_num) {
		this.cct_num = cct_num;
	}
	public String getSrc_chart_fld() {
		return src_chart_fld;
	}
	public void setSrc_chart_fld(String src_chart_fld) {
		this.src_chart_fld = src_chart_fld;
	}
	public String getEng_src_cd() {
		return eng_src_cd;
	}
	public void setEng_src_cd(String eng_src_cd) {
		this.eng_src_cd = eng_src_cd;
	}
	public String getEnt_mthd_cd() {
		return ent_mthd_cd;
	}
	public void setEnt_mthd_cd(String ent_mthd_cd) {
		this.ent_mthd_cd = ent_mthd_cd;
	}
	public String getProd_chart_fld() {
		return prod_chart_fld;
	}
	public void setProd_chart_fld(String prod_chart_fld) {
		this.prod_chart_fld = prod_chart_fld;
	}
	public String getDet_prod_cd() {
		return det_prod_cd;
	}
	public void setDet_prod_cd(String det_prod_cd) {
		this.det_prod_cd = det_prod_cd;
	}
	public String getCust_typ_chtfld() {
		return cust_typ_chtfld;
	}
	public void setCust_typ_chtfld(String cust_typ_chtfld) {
		this.cust_typ_chtfld = cust_typ_chtfld;
	}
	public String getDet_cust_type_cd() {
		return det_cust_type_cd;
	}
	public void setDet_cust_type_cd(String det_cust_type_cd) {
		this.det_cust_type_cd = det_cust_type_cd;
	}
	public String getCust_portfolio_cd() {
		return cust_portfolio_cd;
	}
	public void setCust_portfolio_cd(String cust_portfolio_cd) {
		this.cust_portfolio_cd = cust_portfolio_cd;
	}
	public String getBook_cd_chart_fld() {
		return book_cd_chart_fld;
	}
	public void setBook_cd_chart_fld(String book_cd_chart_fld) {
		this.book_cd_chart_fld = book_cd_chart_fld;
	}
	public String getLedger_cd() {
		return ledger_cd;
	}
	public void setLedger_cd(String ledger_cd) {
		this.ledger_cd = ledger_cd;
	}
	public String getAcct_chart_fld() {
		return acct_chart_fld;
	}
	public void setAcct_chart_fld(String acct_chart_fld) {
		this.acct_chart_fld = acct_chart_fld;
	}
	public String getChnl_cd() {
		return chnl_cd;
	}
	public void setChnl_cd(String chnl_cd) {
		this.chnl_cd = chnl_cd;
	}
	public String getSrc_curr_cd() {
		return src_curr_cd;
	}
	public void setSrc_curr_cd(String src_curr_cd) {
		this.src_curr_cd = src_curr_cd;
	}
	public String getSrc_curr_typ_cd() {
		return src_curr_typ_cd;
	}
	public void setSrc_curr_typ_cd(String src_curr_typ_cd) {
		this.src_curr_typ_cd = src_curr_typ_cd;
	}
	public String getTgt_curr_cd() {
		return tgt_curr_cd;
	}
	public void setTgt_curr_cd(String tgt_curr_cd) {
		this.tgt_curr_cd = tgt_curr_cd;
	}
	public String getTgt_curr_typ_cd() {
		return tgt_curr_typ_cd;
	}
	public void setTgt_curr_typ_cd(String tgt_curr_typ_cd) {
		this.tgt_curr_typ_cd = tgt_curr_typ_cd;
	}
	public String getCr_db_ind() {
		return cr_db_ind;
	}
	public void setCr_db_ind(String cr_db_ind) {
		this.cr_db_ind = cr_db_ind;
	}
	public boolean equals(Id001KeyBean pad001Priv){

		
		if(String.valueOf(this.cust_id).equals(String.valueOf(pad001Priv.getCust_id()))
		 && this.acct_chart_fld.toString().equals(pad001Priv.getAcct_chart_fld().toString())
		 && this.cct_num.toString().equals(pad001Priv.getCct_num().toString())
		 && this.chnl_cd.toString().equals(pad001Priv.getChnl_cd().toString())
		/* && this.cr_db_ind.toString().equals(pad001Priv.getCr_db_ind().toString())*/
		 && this.cust_det_prod_cd.toString().equals(pad001Priv.getCust_det_prod_cd().toString())
		 && this.cust_ext_cd.toString().equals(pad001Priv.getCust_ext_cd().toString())
		 && this.cust_local_num.toString().equals(pad001Priv.getCust_local_num().toString())
		 && this.cust_local_srs_sys_cd.toString().equals(pad001Priv.getCust_local_src_sys_cd().toString())
		 && this.cust_orig_chnl_cd.toString().equals(pad001Priv.getCust_orig_chnl_cd().toString())
		 && this.cust_portfolio_cd.toString().equals(pad001Priv.getCust_portfolio_cd().toString())
		 && this.cust_prim_cust_addr1.toString().equals(pad001Priv.getCust_prim_cust_addr1().toString())
		 && this.cust_prim_cust_addr2.toString().equals(pad001Priv.getCust_prim_cust_addr2().toString())
		 && this.cust_prim_cust_addr_city.toString().equals(pad001Priv.getCust_prim_cust_addr_city().toString())
		 && this.cust_prim_cust_addr_ctry.toString().equals(pad001Priv.getCust_prim_cust_addr_ctry().toString())
		 && this.cust_prim_cust_addr_state.toString().equals(pad001Priv.getCust_prim_cust_addr_state().toString())
		 && this.cust_prim_cust_addr_zip.toString().equals(pad001Priv.getCust_prim_cust_addr_zip().toString())
		 && this.cust_prim_cust_fname.toString().equals(pad001Priv.getCust_prim_cust_fname().toString())
		 && this.cust_prim_cust_id.toString().equals(pad001Priv.getCust_prim_cust_id().toString())
		 && this.cust_prim_cust_lname.toString().equals(pad001Priv.getCust_prim_cust_lname().toString())
		 && this.cust_status_cd.toString().equals(pad001Priv.getCust_status_cd().toString())
		 && this.cust_suff_num.toString().equals(pad001Priv.getCust_suff_num().toString())
		 && this.cust_typ_chtfld.toString().equals(pad001Priv.getCust_typ_chtfld().toString())
		 && this.det_cust_type_cd.toString().equals(pad001Priv.getDet_cust_type_cd().toString())
		 && this.det_prod_cd.toString().equals(pad001Priv.getDet_prod_cd().toString())
		 && this.eng_src_cd.toString().equals(pad001Priv.getEng_src_cd().toString())
		 && this.ent_mthd_cd.toString().equals(pad001Priv.getEnt_mthd_cd().toString())
		 && this.ledger_cd.toString().equals(pad001Priv.getLedger_cd().toString())
		 && this.prod_chart_fld.toString().equals(pad001Priv.getProd_chart_fld().toString())
		 && this.rpt_unit_num.toString().equals(pad001Priv.getRpt_unit_num().toString())
		 && this.src_chart_fld.toString().equals(pad001Priv.getSrc_chart_fld().toString())
		 && this.src_curr_cd.toString().equals(pad001Priv.getSrc_curr_cd().toString())
		 && this.src_curr_typ_cd.toString().equals(pad001Priv.getSrc_curr_typ_cd().toString())
		 && this.tgt_curr_cd.toString().equals(pad001Priv.getTgt_curr_cd().toString())
		 && this.tgt_curr_typ_cd.toString().equals(pad001Priv.getTgt_curr_typ_cd().toString())
		 && this.txn_cust_ind.toString().equals(pad001Priv.getTxn_cust_ind().toString())
		 && this.txn_desc.toString().equals(pad001Priv.getTxn_desc().toString())
		 && this.txn_typ_cd.toString().equals(pad001Priv.getTxn_typ_cd().toString())){
			System.out.println(this.txn_typ_cd.toString());
			return true;
		}
		
		return false;
	}
}
