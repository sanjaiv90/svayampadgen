package beans;

import org.apache.spark.sql.Column;

public class TableJoinBean {

	private String toTable;
	private String columnName;
	private String fromPf;
	private String fromTable;
	private Column[] colInLf;
	
	public Column[] getColInLf() {
		return colInLf;
	}
	public void setColInLf(Column[] colInLf) {
		this.colInLf = colInLf;
	}
	public String getToTable() {
		return toTable;
	}
	public void setToTable(String toTable) {
		this.toTable = toTable;
	}
	public String getFromTable() {
		return fromTable;
	}
	public void setFromTable(String fromTable) {
		this.fromTable = fromTable;
	}
	
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getFromPf() {
		return fromPf;
	}
	public void setFromPf(String fromPf) {
		this.fromPf = fromPf;
	}
}
