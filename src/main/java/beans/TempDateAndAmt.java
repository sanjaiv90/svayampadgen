package beans;

import java.math.BigDecimal;
import java.sql.Date;

public class TempDateAndAmt {

	private BigDecimal  txn_amt;
	private Date acct_dt;
	public BigDecimal getTxn_amt() {
		return txn_amt;
	}
	public void setTxn_amt(BigDecimal txn_amt) {
		this.txn_amt = txn_amt;
	}
	public Date getAcct_dt() {
		return acct_dt;
	}
	public void setAcct_dt(Date acct_dt) {
		this.acct_dt = acct_dt;
	}
	
}
