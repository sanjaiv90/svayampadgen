package insertData;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.sql.types.StructField;

import beans.ColumnMetaData;
import scala.collection.Iterator;

public class InsertMetadata {

	public List<ColumnMetaData> setDataInColMetadata(Iterator<StructField> itr) {
		List<ColumnMetaData> info=new ArrayList<ColumnMetaData>();
		int i=0;
		while(itr.hasNext()){
			
			StructField sf=	itr.next();
			ColumnMetaData colInfo=new ColumnMetaData();
			colInfo.setCol_name(sf.name());
		    colInfo.setCol_data_type(sf.dataType().toString());
			colInfo.setIs_Nullable(sf.nullable());
			colInfo.setCol_id("col"+i++);
			colInfo.setCol_length(100);
			colInfo.setCalc_id("Yes");
			colInfo.setEnv_id("Sanjai_Dev");
			colInfo.setCol_desc("Pad Column");
			colInfo.setUpdated_by("Sanjai");
			info.add(colInfo);
			System.out.println(sf.name());
	        System.out.println(sf.dataType());
		}
		return info;
	}
     
     
}
