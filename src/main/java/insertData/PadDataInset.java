package insertData;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import beans.ColumnMetaData;
import beans.Id001DailyBean;
import beans.Id001KeyBean;
import beans.Id001MonthlyBean;
import beans.JoinBean;
import beans.TempDateAndAmt;
import padGen.MetaDataReader;

public class PadDataInset {

	public List<Id001MonthlyBean> createMonthlyData(List<TempDateAndAmt> listTempDataAndAmt,long padKeyId) {
		
		Boolean isUpdate=false;
		List<Id001MonthlyBean> allMonthlyPadSingleKey=new ArrayList<Id001MonthlyBean>();
		Id001MonthlyBean monthly=null;
		Id001MonthlyBean monthlyPriv=null;
		Calendar txnCalenderDate = Calendar.getInstance();
		int currentYear=txnCalenderDate.get(Calendar.YEAR);
		//create Monthly frist for a padKeyId
		txnCalenderDate.setTime(listTempDataAndAmt.get(0).getAcct_dt());
	    monthly=createUpdateMonthly(padKeyId,listTempDataAndAmt.get(0).getTxn_amt(),txnCalenderDate,new Id001MonthlyBean(),isUpdate);
		allMonthlyPadSingleKey.add(monthly);
		monthlyPriv=monthly;
		if(listTempDataAndAmt.size()>1){
			for(int i=1;i<listTempDataAndAmt.size();i++){
				
				int privYear=Integer.parseInt(monthlyPriv.getYear());
				 txnCalenderDate.setTime(listTempDataAndAmt.get(i).getAcct_dt());
				int sequenceYear=txnCalenderDate.get(Calendar.YEAR);
				// same year -> update
				if(sequenceYear==privYear){
					 BigDecimal txnAmount=listTempDataAndAmt.get(i).getTxn_amt();
					 isUpdate=true;
					 monthly=createUpdateMonthly(padKeyId,txnAmount,txnCalenderDate,monthly,true);
					
					 int nextYear=0;
					 if(i+1<listTempDataAndAmt.size())
					 txnCalenderDate.setTime(listTempDataAndAmt.get(i+1).getAcct_dt()); 
					 nextYear=txnCalenderDate.get(Calendar.YEAR);
					 if(nextYear!=Integer.parseInt(monthly.getYear()) || i== listTempDataAndAmt.size()){
						 allMonthlyPadSingleKey.remove(allMonthlyPadSingleKey.size()-1);
						 allMonthlyPadSingleKey.add(monthly); 
					 }
				}
				// add new pad for next year for given data
				 else if(sequenceYear==privYear+1){
					 txnCalenderDate.setTime(listTempDataAndAmt.get(i).getAcct_dt());
					 BigDecimal txnAmount=listTempDataAndAmt.get(i).getTxn_amt();
					 isUpdate=false;
					 monthly=createUpdateMonthly(padKeyId,txnAmount,txnCalenderDate,new Id001MonthlyBean(),isUpdate);
					 monthly.setBegin_bal(monthlyPriv.getCurrent());
					 monthly.setCurrent(monthlyPriv.getCurrent().add(monthly.getCurrent()));
					 allMonthlyPadSingleKey.add(monthly);
					 monthlyPriv=monthly;
				 }
				//generate sequence year pad and new Year Pad
				 else{
					 int nextYear=0;
					 if(i+1<listTempDataAndAmt.size()){
					 txnCalenderDate.setTime(listTempDataAndAmt.get(i).getAcct_dt()); 
					 nextYear=txnCalenderDate.get(Calendar.YEAR);
					 }
					 for(int year=privYear+1;year<nextYear;year++ ){
						 monthly=setZeroInMonthly(new Id001MonthlyBean());
						monthly.setPad_key_id(padKeyId);
						monthly.setBegin_bal(monthlyPriv.getCurrent());
						monthly.setCurrent(monthlyPriv.getCurrent());
						monthly.setYear(String.valueOf(privYear+1));
						monthly.setPad_type_cd("Y");
						allMonthlyPadSingleKey.add(monthly);
						monthlyPriv=monthly;
						
					 }
					 BigDecimal txnAmount=listTempDataAndAmt.get(i).getTxn_amt();
					 isUpdate=false;
					 monthly=createUpdateMonthly(padKeyId,txnAmount,txnCalenderDate,new Id001MonthlyBean(),isUpdate);
					 monthly.setBegin_bal(monthlyPriv.getCurrent());
					 monthly.setCurrent(monthlyPriv.getCurrent().add(monthly.getCurrent()));
					 allMonthlyPadSingleKey.add(monthly);
					 monthlyPriv=monthly;

				 }
			}
		}
		//default create pad to current year
			int monthlyPadYear=txnCalenderDate.get(Calendar.YEAR);
			if(monthlyPadYear<currentYear){
				for(int year=monthlyPadYear+1;year<=currentYear;year++){
					monthly=setZeroInMonthly(new Id001MonthlyBean());
					monthly.setPad_key_id(padKeyId);
					monthly.setBegin_bal(monthlyPriv.getCurrent());
					monthly.setCurrent(monthlyPriv.getCurrent());
					monthly.setYear(String.valueOf(year));
					monthly.setPad_type_cd("Y");
					allMonthlyPadSingleKey.add(monthly);
					
					monthlyPriv=monthly;
				}
			} 	
		//System.out.println(listTempDataAndAmt.get(0).getAcct_dt());
		//System.out.println(monthlyBean.getYear());
		return allMonthlyPadSingleKey;
	} 
	
	public Id001MonthlyBean createUpdateMonthly(long padKeyId, BigDecimal txn_amt, Calendar accDt, Id001MonthlyBean monthly, boolean isUpdate) {
		
		int month=accDt.get(Calendar.MONTH);
		if(!isUpdate){
		monthly.setPad_key_id(padKeyId);
		monthly.setPad_type_cd("Y");
		monthly.setYear(String.valueOf(accDt.get(Calendar.YEAR)));
		    monthly.setM1((month==1)? txn_amt:BigDecimal.ZERO);
		    monthly.setM2((month==2)? txn_amt:BigDecimal.ZERO);
			monthly.setM3((month==3)? txn_amt:BigDecimal.ZERO);
			monthly.setM4((month==4)? txn_amt:BigDecimal.ZERO);
			monthly.setM5((month==5)? txn_amt:BigDecimal.ZERO);
			monthly.setM6((month==6)? txn_amt:BigDecimal.ZERO);
			monthly.setM7((month==7)? txn_amt:BigDecimal.ZERO);
			monthly.setM8((month==8)? txn_amt:BigDecimal.ZERO);
			monthly.setM9((month==9)? txn_amt:BigDecimal.ZERO);
			monthly.setM10((month==10)? txn_amt:BigDecimal.ZERO);
			monthly.setM11((month==11)? txn_amt:BigDecimal.ZERO);
			monthly.setM12((month==12)? txn_amt:BigDecimal.ZERO);
		    monthly.setCurrent(txn_amt);
		    monthly.setBegin_bal(BigDecimal.ZERO);
		
		//update
		}else{
			
			monthly.setM1((month==1)? monthly.getM1().add(txn_amt):monthly.getM1());
			monthly.setM2((month==2)? monthly.getM2().add(txn_amt):monthly.getM2());
			monthly.setM3((month==3)? monthly.getM3().add(txn_amt):monthly.getM3());
			monthly.setM4((month==4)? monthly.getM4().add(txn_amt):monthly.getM4());
			monthly.setM5((month==5)? monthly.getM5().add(txn_amt):monthly.getM5());
			monthly.setM6((month==6)? monthly.getM6().add(txn_amt):monthly.getM6());
			monthly.setM7((month==7)? monthly.getM7().add(txn_amt):monthly.getM7());
			monthly.setM8((month==8)? monthly.getM8().add(txn_amt):monthly.getM8());
			monthly.setM9((month==9)? monthly.getM9().add(txn_amt):monthly.getM9());
			monthly.setM10((month==10)? monthly.getM10().add(txn_amt):monthly.getM10());
			monthly.setM11((month==11)? monthly.getM11().add(txn_amt):monthly.getM11());
			monthly.setM12((month==12)? monthly.getM12().add(txn_amt):monthly.getM12());
			monthly.setCurrent(monthly.getCurrent().add(txn_amt));
		}
		return monthly;
	}
  private Id001MonthlyBean setZeroInMonthly(Id001MonthlyBean monthly){
	  monthly.setM1(BigDecimal.ZERO);
	  monthly.setM2(BigDecimal.ZERO);
	  monthly.setM3(BigDecimal.ZERO);
	  monthly.setM4(BigDecimal.ZERO);
	  monthly.setM5(BigDecimal.ZERO);
	  monthly.setM6(BigDecimal.ZERO);
	  monthly.setM7(BigDecimal.ZERO);
	  monthly.setM8(BigDecimal.ZERO);
	  monthly.setM9(BigDecimal.ZERO);
	  monthly.setM10(BigDecimal.ZERO);
	  monthly.setM11(BigDecimal.ZERO);
	  monthly.setM12(BigDecimal.ZERO);
	return monthly;  
  }
  public List<Id001DailyBean> createDailyData(List<TempDateAndAmt> listTempDataAndAmt,long padKeyId) {
		// TODO Auto-generated method stub
		Boolean isUpdate=false;
		List<Id001DailyBean> allDailyPadSingleKey=new ArrayList<Id001DailyBean>();
		Id001DailyBean daily=null;
		Id001DailyBean dailyPriv=null;
		Calendar txnCalenderDate = Calendar.getInstance();
		int currentYear=txnCalenderDate.get(Calendar.YEAR);
		int currentMonth=txnCalenderDate.get(Calendar.MONTH)+1;
		//create Daily first for a padKeyId
		txnCalenderDate.setTime(listTempDataAndAmt.get(0).getAcct_dt());
		daily=createUpdateDaily(padKeyId,listTempDataAndAmt.get(0).getTxn_amt(),txnCalenderDate,new Id001DailyBean(),isUpdate);
		allDailyPadSingleKey.add(daily);
		dailyPriv=daily;
		if(listTempDataAndAmt.size()>1){
			for(int i=1;i<listTempDataAndAmt.size();i++){
				
				int privYear=Integer.parseInt(dailyPriv.getYear());
				int privMonth=Integer.parseInt(dailyPriv.getMonth());
				 txnCalenderDate.setTime(listTempDataAndAmt.get(i).getAcct_dt());
				int sequenceYear=txnCalenderDate.get(Calendar.YEAR);
				int sequenceMonth=txnCalenderDate.get(Calendar.MONTH)+1;
				// same year -> update
				if(sequenceYear==privYear && privMonth==sequenceMonth ){
					 BigDecimal txnAmount=listTempDataAndAmt.get(i).getTxn_amt();
					 isUpdate=true;
					 daily=createUpdateDaily(padKeyId,txnAmount,txnCalenderDate,daily,true);
					
					 int nextYear=0;
					 int nextMonth=0;
					 if(i+1<listTempDataAndAmt.size()){
					 txnCalenderDate.setTime(listTempDataAndAmt.get(i+1).getAcct_dt()); 
					 nextYear=txnCalenderDate.get(Calendar.YEAR);
					 nextMonth=txnCalenderDate.get(Calendar.MONTH)+1;
					 }
					 if((nextYear!=Integer.parseInt(daily.getYear()) && nextMonth!=Integer.parseInt(daily.getMonth()) )|| i== listTempDataAndAmt.size()){
						 allDailyPadSingleKey.remove(allDailyPadSingleKey.size()-1);
						 allDailyPadSingleKey.add(daily); 
					 }
				}
				// add new pad for next month for given data
				 else if(sequenceYear==privYear  && sequenceMonth==privMonth+1){
					 txnCalenderDate.setTime(listTempDataAndAmt.get(i).getAcct_dt());
					 BigDecimal txnAmount=listTempDataAndAmt.get(i).getTxn_amt();
					 isUpdate=false;
					 daily=createUpdateDaily(padKeyId,txnAmount,txnCalenderDate,new Id001DailyBean(),isUpdate);
					 allDailyPadSingleKey.add(daily);
					 dailyPriv=daily;
				 }
				//generate sequence year pad and new Year Pad
				 else{
					 Calendar privDate=Calendar.getInstance();
					 privDate.setTime(listTempDataAndAmt.get(i-1).getAcct_dt());
					 privDate.add(Calendar.MONTH, 1);
					 while(txnCalenderDate.compareTo(privDate)>=0){
						 daily=setZeroInDaily(new Id001DailyBean());
							daily.setPad_key_id(padKeyId);
							daily.setYear(String.valueOf(privDate.get(Calendar.YEAR)));
							daily.setMonth(String.valueOf(privDate.get(Calendar.MONTH)+1));
							daily.setPad_type_cd("M");
							allDailyPadSingleKey.add(daily);
							/*dailyPriv=daily;*/
							privDate.add(Calendar.MONTH,1); 
					 }
					 allDailyPadSingleKey.remove(allDailyPadSingleKey.size()-1);
					 BigDecimal txnAmount=listTempDataAndAmt.get(i).getTxn_amt();
					 isUpdate=false;
					 daily=createUpdateDaily(padKeyId,txnAmount,txnCalenderDate,new Id001DailyBean(),isUpdate);
					 allDailyPadSingleKey.add(daily);
					 dailyPriv=daily;

				 }
			}
		}
		//default create pad to current year and month
			txnCalenderDate.add(Calendar.MONTH,1);
			while(Calendar.getInstance().compareTo(txnCalenderDate)>=0){
				
					daily=setZeroInDaily(new Id001DailyBean());
					daily.setPad_key_id(padKeyId);
					daily.setYear(String.valueOf(txnCalenderDate.get(Calendar.YEAR)));
					daily.setMonth(String.valueOf(txnCalenderDate.get(Calendar.MONTH)+1));
					daily.setPad_type_cd("M");
					allDailyPadSingleKey.add(daily);
					/*dailyPriv=daily;*/
					txnCalenderDate.add(Calendar.MONTH,1);
				}
		//System.out.println(listTempDataAndAmt.get(0).getAcct_dt());
		//System.out.println(monthlyBean.getYear());
		return allDailyPadSingleKey;
	} 
  
  public Id001DailyBean createUpdateDaily(long padKeyId, BigDecimal txn_amt, Calendar txnCalenderDate, Id001DailyBean daily, boolean isUpdate) {
		
		 int day=txnCalenderDate.get(Calendar.DAY_OF_MONTH);
		 if(!isUpdate){
			 daily.setPad_key_id(padKeyId);
			 daily.setPad_type_cd("M");
			 daily.setYear(String.valueOf(txnCalenderDate.get(Calendar.YEAR)));
			 daily.setMonth(String.valueOf(txnCalenderDate.get(Calendar.MONTH)+1));
			
			  daily.setD1((day==1)? txn_amt:BigDecimal.ZERO);
			  daily.setD2((day==2)? txn_amt:BigDecimal.ZERO);
			  daily.setD3((day==3)? txn_amt:BigDecimal.ZERO);
			  daily.setD4((day==4)? txn_amt:BigDecimal.ZERO);
			  daily.setD5((day==5)? txn_amt:BigDecimal.ZERO);
			  daily.setD6((day==6)? txn_amt:BigDecimal.ZERO);
			  daily.setD7((day==7)? txn_amt:BigDecimal.ZERO);
			  daily.setD8((day==8)? txn_amt:BigDecimal.ZERO);
			  daily.setD9((day==9)? txn_amt:BigDecimal.ZERO);
			  daily.setD10((day==10)? txn_amt:BigDecimal.ZERO);
			  daily.setD11((day==11)? txn_amt:BigDecimal.ZERO);
			  daily.setD12((day==12)? txn_amt:BigDecimal.ZERO);
			  daily.setD13((day==13)? txn_amt:BigDecimal.ZERO);
			  daily.setD14((day==14)? txn_amt:BigDecimal.ZERO);
			  daily.setD15((day==15)? txn_amt:BigDecimal.ZERO);
			  daily.setD16((day==16)? txn_amt:BigDecimal.ZERO);
			  daily.setD17((day==17)? txn_amt:BigDecimal.ZERO);
			  daily.setD18((day==18)? txn_amt:BigDecimal.ZERO);
			  daily.setD19((day==19)? txn_amt:BigDecimal.ZERO);
			  daily.setD20((day==20)? txn_amt:BigDecimal.ZERO);
			  daily.setD21((day==21)? txn_amt:BigDecimal.ZERO);
			  daily.setD22((day==22)? txn_amt:BigDecimal.ZERO);
			  daily.setD23((day==23)? txn_amt:BigDecimal.ZERO);
			  daily.setD24((day==24)? txn_amt:BigDecimal.ZERO);
			  daily.setD25((day==25)? txn_amt:BigDecimal.ZERO);
			  daily.setD26((day==26)? txn_amt:BigDecimal.ZERO);
			  daily.setD27((day==27)? txn_amt:BigDecimal.ZERO);
			  daily.setD28((day==28)? txn_amt:BigDecimal.ZERO);
			  daily.setD29((day==29)? txn_amt:BigDecimal.ZERO);
			  daily.setD30((day==30)? txn_amt:BigDecimal.ZERO);
			  daily.setD31((day==31)? txn_amt:BigDecimal.ZERO);
			  return daily;
	}else{
		   daily.setD1((day==1)? daily.getD1().add(txn_amt):daily.getD1());
		   daily.setD2((day==2)? daily.getD2().add(txn_amt):daily.getD2());
		   daily.setD3((day==3)? daily.getD3().add(txn_amt):daily.getD3());
		   daily.setD4((day==4)? daily.getD4().add(txn_amt):daily.getD4());
		   daily.setD5((day==5)? daily.getD5().add(txn_amt):daily.getD5());
		   daily.setD6((day==6)? daily.getD6().add(txn_amt):daily.getD6());
		   daily.setD7((day==7)? daily.getD7().add(txn_amt):daily.getD7());
		   daily.setD8((day==8)? daily.getD8().add(txn_amt):daily.getD8());
		   daily.setD9((day==9)? daily.getD9().add(txn_amt):daily.getD9());
		   daily.setD10((day==10)? daily.getD10().add(txn_amt):daily.getD10());
		   daily.setD11((day==11)? daily.getD11().add(txn_amt):daily.getD11());
		   daily.setD12((day==12)? daily.getD12().add(txn_amt):daily.getD12());
		   daily.setD13((day==13)? daily.getD13().add(txn_amt):daily.getD13());
		   daily.setD14((day==14)? daily.getD14().add(txn_amt):daily.getD14());
		   daily.setD15((day==15)? daily.getD15().add(txn_amt):daily.getD15());
		   daily.setD16((day==16)? daily.getD16().add(txn_amt):daily.getD16());
		   daily.setD17((day==17)? daily.getD17().add(txn_amt):daily.getD17());
		   daily.setD18((day==18)? daily.getD18().add(txn_amt):daily.getD18());
		   daily.setD19((day==19)? daily.getD19().add(txn_amt):daily.getD19());
		   daily.setD20((day==20)? daily.getD20().add(txn_amt):daily.getD20());
		   daily.setD21((day==21)? daily.getD21().add(txn_amt):daily.getD21());
		   daily.setD22((day==22)? daily.getD22().add(txn_amt):daily.getD22());
		   daily.setD23((day==23)? daily.getD23().add(txn_amt):daily.getD23());
		   daily.setD24((day==24)? daily.getD24().add(txn_amt):daily.getD24());
		   daily.setD25((day==25)? daily.getD25().add(txn_amt):daily.getD25());
		   daily.setD26((day==26)? daily.getD26().add(txn_amt):daily.getD26());
		   daily.setD27((day==27)? daily.getD27().add(txn_amt):daily.getD27());
		   daily.setD28((day==28)? daily.getD28().add(txn_amt):daily.getD28());
		   daily.setD29((day==29)? daily.getD29().add(txn_amt):daily.getD29());
		   daily.setD30((day==30)? daily.getD30().add(txn_amt):daily.getD30());
		   daily.setD31((day==31)? daily.getD31().add(txn_amt):daily.getD31());
				
		   return daily;
			  }
		
	}
  public Id001DailyBean setZeroInDaily(Id001DailyBean daily){
		 daily.setD1(BigDecimal.ZERO);
		 daily.setD2(BigDecimal.ZERO);
		 daily.setD3(BigDecimal.ZERO);
		 daily.setD4(BigDecimal.ZERO);
		 daily.setD5(BigDecimal.ZERO);
		 daily.setD6(BigDecimal.ZERO);
		 daily.setD7(BigDecimal.ZERO);
		 daily.setD8(BigDecimal.ZERO);
		 daily.setD9(BigDecimal.ZERO);
		 daily.setD10(BigDecimal.ZERO);
		 daily.setD11(BigDecimal.ZERO);
		 daily.setD12(BigDecimal.ZERO);
		 daily.setD13(BigDecimal.ZERO);
		 daily.setD14(BigDecimal.ZERO);
		 daily.setD15(BigDecimal.ZERO);
		 daily.setD16(BigDecimal.ZERO);
		 daily.setD17(BigDecimal.ZERO);
		 daily.setD18(BigDecimal.ZERO);
		 daily.setD19(BigDecimal.ZERO);
		 daily.setD20(BigDecimal.ZERO);
		 daily.setD21(BigDecimal.ZERO);
		 daily.setD22(BigDecimal.ZERO);
		 daily.setD23(BigDecimal.ZERO);
		 daily.setD24(BigDecimal.ZERO);
		 daily.setD25(BigDecimal.ZERO);
		 daily.setD26(BigDecimal.ZERO);
		 daily.setD27(BigDecimal.ZERO);
		 daily.setD28(BigDecimal.ZERO);
		 daily.setD29(BigDecimal.ZERO);
		 daily.setD30(BigDecimal.ZERO);
		 daily.setD31(BigDecimal.ZERO);
		return daily;  
	  }
	/*static long idCounter = 1;
	private static synchronized long getPad001Key() {
		
		return idCounter++;
	}*/
	 /* 
		@SuppressWarnings("deprecation")
		public  void setPadData(List<JoinBean> pad001Bean, SQLContext sqlContext, String url) {
			List<Id001KeyBean> pad001List=new ArrayList<Id001KeyBean>();
			List<Id001MonthlyBean> pad001MonthlyList=new ArrayList<Id001MonthlyBean>();
			List<Id001DailyBean> pad001DailyList=new ArrayList<Id001DailyBean>();
			List<TempDateAndAmt> listTempDataAndAmt=null;
			Id001MonthlyBean monthlyBean=null;
			Id001KeyBean pad001Priv=null;
			long padKeyId=0;
			for(int i=0;i<pad001Bean.size();i++){
				Id001KeyBean pad001=new Id001KeyBean();
			//	System.out.println(pad001Bean.get(i).getCust_id()+"-"+pad001Bean.get(i).getCust_prim_cust_fname()+"-"+pad001Bean.get(i).getTxn_amt());
				//pad001.setPad_key_id(1);

				pad001.setCust_id(pad001Bean.get(i).getCust_id());
				pad001.setAcct_chart_fld(pad001Bean.get(i).getAcct_chart_fld());
				pad001.setBook_cd_chart_fld(pad001Bean.get(i).getBook_cd_chart_fld());
				pad001.setCct_num(pad001Bean.get(i).getCct_num());
				pad001.setChnl_cd(pad001Bean.get(i).getChnl_cd());
				pad001.setCr_db_ind(pad001Bean.get(i).getCr_db_ind());
				pad001.setCust_det_prod_cd(pad001Bean.get(i).getCust_det_prod_cd());
				pad001.setCust_ext_cd(pad001Bean.get(i).getCust_ext_cd());
				pad001.setCust_local_num(pad001Bean.get(i).getCust_local_num());
				pad001.setCust_local_src_sys_cd(pad001Bean.get(i).getCust_local_src_sys_cd());
				pad001.setCust_orig_chnl_cd(pad001Bean.get(i).getCust_orig_chnl_cd());
				pad001.setCust_portfolio_cd(pad001Bean.get(i).getCust_portfolio_cd());
				//pad001.setCust_prim_cust_id(pad001Bean.get(i).getCust_prim_cust_id())
				pad001.setCust_prim_cust_addr1(pad001Bean.get(i).getCust_prim_cust_addr1());
				pad001.setCust_prim_cust_addr2(pad001Bean.get(i).getCust_prim_cust_addr2());
				pad001.setCust_prim_cust_addr_city(pad001Bean.get(i).getCust_prim_cust_addr_city());
				pad001.setCust_prim_cust_addr_ctry(pad001Bean.get(i).getCust_prim_cust_addr_ctry());
				pad001.setCust_prim_cust_addr_state(pad001Bean.get(i).getCust_prim_cust_addr_state());
				pad001.setCust_prim_cust_addr_zip(pad001Bean.get(i).getCust_prim_cust_addr_zip());
				pad001.setCust_prim_cust_fname(pad001Bean.get(i).getCust_prim_cust_fname());
				pad001.setCust_prim_cust_lname(pad001Bean.get(i).getCust_prim_cust_lname());
				pad001.setCust_prim_cust_id(pad001Bean.get(i).getCust_prim_cust_id());
				pad001.setCust_status_cd(pad001Bean.get(i).getCust_status_cd());
				pad001.setCust_suff_num(pad001Bean.get(i).getCust_suff_num());
				pad001.setCust_typ_chtfld(pad001Bean.get(i).getCust_typ_chtfld());
				pad001.setDet_cust_type_cd(pad001Bean.get(i).getDet_cust_type_cd());
				pad001.setDet_prod_cd(pad001Bean.get(i).getCust_det_prod_cd());
				pad001.setEng_src_cd(pad001Bean.get(i).getEng_src_cd());
				pad001.setEnt_mthd_cd(pad001Bean.get(i).getEnt_mthd_cd());
				pad001.setLedger_cd(pad001Bean.get(i).getLedger_cd());
				pad001.setProd_chart_fld(pad001Bean.get(i).getProd_chart_fld());
				pad001.setRpt_unit_num(pad001Bean.get(i).getRpt_unit_num());
				pad001.setSrc_chart_fld(pad001Bean.get(i).getSrc_chart_fld());
				pad001.setSrc_curr_cd(pad001Bean.get(i).getSrc_curr_cd());
				pad001.setSrc_curr_typ_cd(pad001Bean.get(i).getSrc_curr_typ_cd());
				pad001.setTgt_curr_cd(pad001Bean.get(i).getTgt_curr_cd());
				pad001.setTgt_curr_typ_cd(pad001Bean.get(i).getTgt_curr_typ_cd());
				pad001.setTxn_cust_ind(pad001Bean.get(i).getTxn_cust_ind());
				pad001.setTxn_desc(pad001Bean.get(i).getTxn_desc());
				pad001.setTxn_typ_cd(pad001Bean.get(i).getTxn_typ_cd());
				System.out.println(pad001Bean.get(i).getCust_prim_cust_id());
				
				
				if(i==0 || !pad001.equals(pad001Priv)){
					//create New PadID
					
					
				     if(listTempDataAndAmt!=null){
					 pad001MonthlyList.addAll(createMonthlyData(listTempDataAndAmt,padKeyId));
					 pad001DailyList.addAll(createDailyData(listTempDataAndAmt,padKeyId));
				     }
				  //create Data in Monthly
				     monthlyBean=createNewMonthly(padKeyId,pad001Bean.get(i).getTxn_amt(),pad001Bean.get(i).getAcct_dt());
					pad001MonthlyList.add(monthlyBean);
					
					
					//create Data in Daily
					Id001DailyBean daily=createNewDaily(padKeyId,pad001Bean.get(i).getTxn_amt(),pad001Bean.get(i).getAcct_dt());
					pad001DailyList.add(daily);
					
					//set  pad key in Pad Table
					padKeyId=getPad001Key();
					pad001.setPad_key_id(padKeyId);
					pad001List.add(pad001);	
					listTempDataAndAmt=new ArrayList<TempDateAndAmt>();
				}else{
					//pad already exits
					System.out.println("Key not break");
					
				}
				TempDateAndAmt tempDateAndAmt=new TempDateAndAmt();
				tempDateAndAmt.setAcct_dt(pad001Bean.get(i).getAcct_dt());
				tempDateAndAmt.setTxn_amt(pad001Bean.get(i).getTxn_amt());
				listTempDataAndAmt.add(tempDateAndAmt);
				
				pad001Priv=pad001;
		}
			DataFrame dfPad001Daily = sqlContext.createDataFrame(pad001DailyList, Id001DailyBean.class);
			DataFrame dfPad001Monthly = sqlContext.createDataFrame(pad001MonthlyList, Id001MonthlyBean.class);
			DataFrame dfPad001Key = sqlContext.createDataFrame(pad001List, Id001KeyBean.class);
		
			dfPad001Key.insertIntoJDBC(url, "id001_pad_key1", false);
			dfPad001Monthly.insertIntoJDBC(url, "id001_pad_monthly1", false);
			dfPad001Daily.insertIntoJDBC(url, "id001_pad_daily1", false);
			//dfPad001Daily.show(100);
			
			StructType st= dfPad001Key.schema();
			String ss=st.mkString();
			System.out.println(ss);
			scala.collection.Iterator<StructField> itr=st.iterator();
			SetPadMetaData utilPadMeta=new SetPadMetaData();
			List<ColumnMetaData> info=utilPadMeta.setDataInColMetadata(itr);
			
			
			System.out.println(info.size());
			DataFrame colMataDf = sqlContext.createDataFrame(info, ColumnMetaData.class);
			
			System.out.println(colMataDf.sqlContext().catalog());
			//colMataDf.registerTempTable("column");
			colMataDf.write().mode(SaveMode.Overwrite).saveAsTable("column");
			java.util.Properties connectionProperties=new Properties();
			connectionProperties.setProperty("SaveMode", "Overwrite");
			colMataDf.write().jdbc(url, "column", connectionProperties);
			//colMataDf.persist();
			
			colMataDf.insertIntoJDBC(url, "columns", false);
			
		}*/
}
