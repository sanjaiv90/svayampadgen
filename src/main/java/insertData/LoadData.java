package insertData;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.spark.sql.Column;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;

public class LoadData {
	
	@SuppressWarnings("deprecation")
	public void loadDataFromDsToDs(String fromUrl,String toUrl,String fromTable,String toTable,Set<String> colName, SQLContext sqlContext){
		Column[] col=new Column[colName.size()];
		int i=0;
		for(String s:colName){
			col[i]=new Column(s);
			i++;
		}
		Map<String,String> conOptions=new  HashMap<String,String>();
		conOptions.put("url", fromUrl);
		 DataFrame dfLoad=sqlContext.read()
			        .format("jdbc")
			        .options(conOptions)
			        .option("dbtable",fromTable)
			        .load().select(col);
		 dfLoad.show();
		 dfLoad.insertIntoJDBC(toUrl, toTable, true);
		 
		 System.out.println("*******"+toTable+" Sucessfull **************");
		 
	}

}
