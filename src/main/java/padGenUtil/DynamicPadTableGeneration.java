package padGenUtil;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.spark.sql.Column;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import beans.CalcColumnBean;
import beans.Id001DailyBean;
import beans.Id001MonthlyBean;
import beans.TempDateAndAmt;
import insertData.PadDataInset;

public class DynamicPadTableGeneration {
	
	@SuppressWarnings("deprecation")
	public long createPad(DataFrame joinData,SQLContext sqlContext,String url, List<String> createPadList, String padId, CalcColumnBean calcBean){ 
	System.out.println(padId);
		StructType schemas =joinData.schema();
		StructField[] sf=schemas.fields();
		String[] columns=joinData.columns();
	    Set<String> columnSet = new HashSet<String>(Arrays.asList(columns));
	    System.out.println(columnSet);
		Column[] listPadColumn=new Column[createPadList.size()];
		StructField[] newSf=new StructField[listPadColumn.length+1];
		Column[] listOrderBy =new Column[listPadColumn.length+1];
		//final List<String> orderBy=new ArrayList<String>();
		int j=0, m=0;
		for(int i=0;i<columns.length;i++){
			if(createPadList.contains(columns[i])){
			 listPadColumn[j]=new Column(columns[i]);
			 listOrderBy[j]=new Column(columns[i]);
			 newSf[j]=sf[i];
			 j++;
			}
		}
		/*for(String cols:columnSet){
			
			if(createPadList.contains(cols)){
				 listPadColumn[j]=new Column(cols);
				 listOrderBy[j]=new Column(cols);
				 newSf[j]=sf[m];
				m++; j++;
			}
		}*/
		
		listOrderBy[j]=new Column(calcBean.getDateCol());
		//joinData.orderBy(listOrderBy);
		List<Row> pad=new ArrayList<Row>();
		List<Id001MonthlyBean> pad001MonthlyList=new ArrayList<Id001MonthlyBean>();
		List<Id001DailyBean> pad001DailyList=new ArrayList<Id001DailyBean>();
		List<TempDateAndAmt> listTempDataAndAmt=null;
		
		long padKeyId=0;
		Row[] arrJoin=joinData.orderBy(listOrderBy).collect();
		PadDataInset padInserts=new PadDataInset();
		Object[] objRowPriv=new Object[listPadColumn.length+1];
		for(int i=0;i<arrJoin.length;i++){
			    Object[] objRow=new Object[listPadColumn.length+1];
	    	    for(int col=0;col<listPadColumn.length;col++){
	    	    	objRow[col]=arrJoin[i].getAs(listPadColumn[col].toString());
	    	   }
	    	    
	    	    if(!Arrays.equals(objRow,objRowPriv)){
	    	    	 if(listTempDataAndAmt!=null){
	    				 pad001MonthlyList.addAll(padInserts.createMonthlyData(listTempDataAndAmt,padKeyId));
	    				 pad001DailyList.addAll(padInserts.createDailyData(listTempDataAndAmt,padKeyId));
	    			     }
	    				
	    	    	 objRowPriv=objRow.clone();
	    	    	 padKeyId=getPad001Key();
	    	    	 objRow[j]=padKeyId;
	    	    	 Row padRow=RowFactory.create(objRow);
	    	    	 pad.add(padRow);
	    	    	 listTempDataAndAmt=new ArrayList<TempDateAndAmt>();
	    	    	 
	    		}else{
	    				/*//pad already exits
	    				System.out.println("row are same");
	    				 objRowPriv=objRow.clone();*/
	    	         }
	    	   
	    			TempDateAndAmt tempDateAndAmt=new TempDateAndAmt();
	    			tempDateAndAmt.setAcct_dt((Date) arrJoin[i].getAs(calcBean.getDateCol()));
	    			tempDateAndAmt.setTxn_amt((BigDecimal) arrJoin[i].getAs(calcBean.getAmtCol()));
	    			listTempDataAndAmt.add(tempDateAndAmt);
	    	         if(i==arrJoin.length-1){
		    				 pad001MonthlyList.addAll(padInserts.createMonthlyData(listTempDataAndAmt,padKeyId));
		    				 pad001DailyList.addAll(padInserts.createDailyData(listTempDataAndAmt,padKeyId)); 
	    	         }
	    	   
	    	   }
		
		      newSf[j]=DataTypes.createStructField("pad_key_id", DataTypes.LongType, true);
			  StructType newSchema=new StructType(newSf);
		 
	         DataFrame padData=sqlContext.createDataFrame(pad, newSchema);
	         DataFrame dfPad001Daily = sqlContext.createDataFrame(pad001DailyList, Id001DailyBean.class);
	 		 DataFrame dfPad001Monthly = sqlContext.createDataFrame(pad001MonthlyList, Id001MonthlyBean.class);
	 		
	 	
	 		padData.insertIntoJDBC(url, padId, true);
	 		dfPad001Monthly.insertIntoJDBC(url, padId+"_monthly", true);
	 		dfPad001Daily.insertIntoJDBC(url,  padId+"_daily", true);
	 		idCounter=1;
	 		//padData.show(400);
		     /* System.out.println("*********"+padData.count());
	          System.out.println("*****************************"+arrJoin.length);*/
	            
		return padData.count();
		}
		static long idCounter = 1;
		private static synchronized long getPad001Key() {
			
			return idCounter++;
		}
}
