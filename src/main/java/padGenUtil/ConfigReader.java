package padGenUtil;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import beans.DriverUriBean;

public class ConfigReader {
	 @SuppressWarnings("unused")
	public  DriverUriBean getDbUrlFormConfig() throws IOException{
    	 String url="";
    	 Properties prop=new Properties();
    	 String propFile="config.properties";
    	 InputStream inSteam=new FileInputStream(propFile);
    	 if(inSteam!=null)
    		 prop.load(inSteam);
    	 else {
				throw new FileNotFoundException("property file '" + inSteam + "' not found in the classpath");
		      }
    	 DriverUriBean driverUrl=new DriverUriBean();
    	 url=prop.getProperty("url") +prop.getProperty("db")+"?user="+prop.getProperty("user")+"&password="+prop.getProperty("password");
    	 driverUrl.setUrl(url);
    	 driverUrl.setDriver(prop.getProperty("driver"));
    	 return driverUrl;
       }
}
