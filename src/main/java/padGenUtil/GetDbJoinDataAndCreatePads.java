package padGenUtil;
 
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.spark.sql.Column;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;

import beans.CalcColumnBean;
import beans.TableJoinBean;
import scala.collection.immutable.Seq;

public class GetDbJoinDataAndCreatePads {
	
	public  long padGen(List<TableJoinBean> listTableJoin, CalcColumnBean calcBean, Set<String> padList, 
			String toUrl, SQLContext sqlContext, String padId)  throws IOException {
		//ConfigReader classObj=new ConfigReader();
		//String url=classObj.getDbUrlFormConfig();
		//String url="jdbc:mysql://localhost:3306/svayam_dev?user=root&password=fun2learn";
		//Map<String,String> conOptions=new  HashMap<String,String>();
		//conOptions.put("url", url);
		/*conOptions.put("user","root");
		  conOptions.put("password", "fun2learn");*/
		
			 DataFrame df1=sqlContext.read()
				        .format("jdbc")
				        .option("url",listTableJoin.get(0).getFromPf())
				        .option("dbtable",listTableJoin.get(0).getFromTable())
				        .load().select(listTableJoin.get(0).getColInLf());
			 for(int i=1;i<listTableJoin.size();i++){
				 DataFrame joinDf=sqlContext.read()
					        .format("jdbc")
					        .option("url", listTableJoin.get(i).getFromPf())
					        .option("dbtable",listTableJoin.get(i).getFromTable())
					        .load().select(listTableJoin.get(i).getColInLf());
				// df2.show();
				/* df1=df1.join(df2,df1.col(listTableJoin.get(i-1).getColumnName()).equalTo(df2.col(listTableJoin.get(i).getColumnName())))
						 .drop(df2.col(listTableJoin.get(i).getColumnName()));
				 */
				 df1=df1.join(joinDf, listTableJoin.get(i).getColumnName());
				 
				// df1.show();
		    }
		 /*DataFrame custDF=sqlContext.read()
			        .format("jdbc")
			        .options(conOptions)
			        .option("dbtable","customer")
			        .load();
		 
	DataFrame joinData=sqlContext.read()
		       .format("jdbc")
		        .options(conOptions)
		        .option("dbtable","transaction")
		        .load().join(custDF, "cust_id")
		      //  .drop(custDF.col("cust_local_num")).drop(custDF.col("cust_suff_num"))
		        .dropDuplicates();*/
			// df1.show();
	/*System.exit(0);*/
	//MetaDataReader metaData=new MetaDataReader();
			// String[] duplicate={"cust_local_num"};
	List<String> createPadList=new ArrayList<String>();
	createPadList.addAll(padList);
	DynamicPadTableGeneration dpg=new DynamicPadTableGeneration();
	long padcount= dpg.createPad(df1,sqlContext,toUrl,createPadList,padId,calcBean);
	return padcount;
	
	}
	/*StructType schema = DataTypes
	.createStructType(new StructField[] {
			DataTypes.createStructField("id", DataTypes.IntegerType, false),
			DataTypes.createStructField("name", DataTypes.StringType, false),
			DataTypes.createStructField("url", DataTypes.StringType, true),
			DataTypes.createStructField("pictures", DataTypes.StringType, true),
			DataTypes.createStructField("pictures", DataTypes.StringType, true),
			DataTypes.createStructField("time", DataTypes., true) });*/

//StructType st=StructType(Array(StructField("firstName",org.apache.spark.sql.types.StringType,true),StructField("lastName",StringType,true),StructField("age",IntegerType,true)));
	//joinData.show();
		/*Encoder<JoinBean> pad001Encoder = Encoders.bean(JoinBean.class);
		Dataset<JoinBean> pad001BeanDS = new Dataset<JoinBean>(sqlContext,joinData.logicalPlan(), pad001Encoder);
		final List<JoinBean> pad001Bean=pad001BeanDS.collectAsList();*/
		/*PadDataInset objpadDataInSet=new PadDataInset();
		objpadDataInSet.setPadData(pad001Bean,sqlContext,url);
		DataFrame df = sqlContext.createDataFrame(pad001Bean, JoinBean.class);*/
		// final List<Row> listRow =null;
	/* .orderBy("cust_id","cust_local_srs_sys_cd","cust_local_num","cust_suff_num","cust_eff_dt","cust_wrk_dt","cust_proc_dt",
	"cust_status_cd","cust_ext_cd","cust_orig_chnl_cd","cust_det_prod_cd","cust_prim_cust_fname","cust_prim_cust_lname",      		
	"cust_prim_cust_id","cust_prim_cust_addr1","cust_prim_cust_addr2","cust_prim_cust_addr_city","cust_prim_cust_addr_state",
	"cust_prim_cust_addr_ctry","cust_prim_cust_addr_zip","txn_desc","txn_typ_cd","txn_cust_ind","cust_local_srs_sys_cd",
		"cust_local_num","cust_suff_num","rpt_unit_num","cct_num","src_chart_fld","eng_src_cd","ent_mthd_cd","prod_chart_fld",
	"det_prod_cd","cust_typ_chtfld","det_cust_type_cd","cust_portfolio_cd","book_cd_chart_fld","ledger_cd","acct_chart_fld",
	"chnl_cd","src_curr_cd","src_curr_typ_cd","tgt_curr_cd","tgt_curr_typ_cd","cr_db_ind","acct_dt");
*/
}
