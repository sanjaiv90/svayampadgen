package padGen;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.SQLContext;

import beans.CalcColumnBean;
import beans.DriverUriBean;
import beans.TableJoinBean;
import insertData.LoadData;
import padGenUtil.ConfigReader;
import padGenUtil.GetDbJoinDataAndCreatePads;

public class MetaDataReader {
	 Connection con=null;
	public static void main(String[] args) throws IOException {
		SparkConf conf=new SparkConf().setAppName("SinglePass2").setMaster("local[2]");
		JavaSparkContext jsc=new JavaSparkContext(conf);
		SQLContext sqlContext = new SQLContext(jsc);
		DriverUriBean driverUrl=null;
		ConfigReader classObj=new ConfigReader();
	    try {
	    	 driverUrl=classObj.getDbUrlFormConfig();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		MetaDataReader mdr=new MetaDataReader();
		mdr.createMetadataDbConnection(driverUrl);
		mdr.getDataSourceReadOperation(sqlContext);
	}
   
	public void createMetadataDbConnection(DriverUriBean driverUrl){
		/*String url = "jdbc:mysql://localhost:3306/"; 
		String db = "svayam_dev";
		String driver = "org.svayam.jdbc.SvayamDriver";*/
		
			try {
				Class.forName(driverUrl.getDriver());
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				con = DriverManager.getConnection(driverUrl.getUrl());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	public ResultSet executeQuery(String query) {
		Statement stmt=null;
		ResultSet rs=null;
		try {
			stmt = con.createStatement();
			rs=stmt.executeQuery(query);
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return rs;
	}
	public int executeUpdate(String query) {
		
		Statement stmt=null;
		int count=0;
		try {
			stmt = con.createStatement();
			count=stmt.executeUpdate(query);
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return count;
	}
	public void getDataSourceReadOperation(SQLContext sqlContext) throws IOException{
		String url="";
		String operationQuery=" select * from operation order by ops ";
		
		ResultSet rs=executeQuery(operationQuery);
		try {
			while(rs.next()){
				
				if(rs.getString("ops").equals("loaddata")){
				   String fromLf=rs.getString("from_lf");
				   String selectListCol="select col_short_name from operation o,lf_col_xref lc,column_dtl c where o.from_lf=lc.lf_id and lc.col_id=c.col_id"
				   		+ " and o.from_lf='"+fromLf+"' and o.ops='loaddata' and is_calculated=0";
				   ResultSet rsColList=executeQuery(selectListCol);
				   Set<String> colName=new HashSet<String>();
				   while(rsColList.next()){
					   colName.add(rsColList.getString(1));
				   }
				   String fromPfId=rs.getString("from_pf");
				    ResultSet rsFromPf=getDataSourcePF(fromPfId);
					String fromUrl=getDataSourceUrlToReadData(rsFromPf);
					rsFromPf.beforeFirst();
					String fromTable=getPfTable(rsFromPf);
					
					System.out.println("fromUrl :"+fromUrl);
					System.out.println("fromTable :"+fromTable);
					String toPfId=rs.getString("to_pf");
					ResultSet rsToPf=getDataSourcePF(toPfId);
					String toUrl=getDataSourceUrlToReadData(rsToPf);
					rsToPf.beforeFirst();
					String toTable=getPfTable(rsToPf);
					System.out.println("toUrl  :"+toUrl);
					System.out.println("toTable  :"+toTable);
					LoadData loadData=new LoadData();
					loadData.loadDataFromDsToDs(fromUrl, toUrl, fromTable, toTable,colName, sqlContext);
					
				  }else if(rs.getString("ops").contains("pad_ops")){
					 List<TableJoinBean> setTableJoin=null;
					 String from_lf=rs.getString("from_lf");
					 String padId=rs.getString("to_lf");
					 String toPfId=rs.getString("to_pf");
					 ResultSet rsToPf=getDataSourcePF(toPfId);
					 String toUrl=getDataSourceUrlToReadData(rsToPf);	 
					setTableJoin=getTableMetaData(from_lf,padId);
					String calc_id=rs.getString("ops");
					CalcColumnBean	calcBean=getCalculationColumn(calc_id);
					Set<String> padList=getPadFieldList(padId);
					
					if(padList.size()>0){
						
					GetDbJoinDataAndCreatePads  objPadgen=new GetDbJoinDataAndCreatePads();
				    long padCount=objPadgen.padGen(setTableJoin,calcBean,padList,toUrl,sqlContext,padId);
				    setPadCountInOperationTable(padCount,padId);
				    
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public String getPfTable(ResultSet rs){
		String table="";
		try {
			while(rs.next()){
				table=rs.getString("pf_name");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return table;
	}
	
	public String getDataSourceUrlToReadData(ResultSet rs){
		String url="";		
		try {
			while(rs.next()){
				url=rs.getString("db_url")+"://"+rs.getString("server")+":"+rs.getString("port")+"/" +rs.getString("schema")+"?user="+rs.getString("user")+"&password="+rs.getString("password");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return url;
	}
	public List<TableJoinBean> getTableMetaData(String from_lf, String padId){
		List<TableJoinBean> tabJoin=new ArrayList<TableJoinBean>();
		if(from_lf.contains("jn")){
		String query="select distinct src_lf_col_xref_id,tgt_lf_col_xref_id,src_pf_lf_xref_id,tgt_pf_lf_xref_id from operation o,join_dtl j,jn_jnstep_xref js,column_join cj where o.from_lf=j.jn_id and  j.jn_id=js.jn_id and js.col_jn_id=cj.col_jn_id and o.from_lf ='"+from_lf+"'";
		ResultSet rs=executeQuery(query);
		try {
			while(rs.next()){
				
				String srcJoinColId=rs.getString("src_lf_col_xref_id");
				String tgtJoinColId=rs.getString("tgt_lf_col_xref_id");
				String srcPf=rs.getString("src_pf_lf_xref_id");
				String tgtPf=rs.getString("tgt_pf_lf_xref_id");
				String tableJoinAllQuery="select * from pf_lf_xref pl inner join physical_file pf on pl.pf_id=pf.pf_id inner join" 
                                         +" pf_ds_xref pds on pf.pf_id=pds.pf_id inner join datasource ds  on pds.ds_id=ds.ds_id inner join "
                                         +"lf_col_xref lfc on pl.lf_id=lfc.lf_id inner join column_dtl cd on lfc.col_id=cd.col_id "
                                         +"where (pl.pf_lf_xref_id ="+srcPf+" or pf_lf_xref_id="+tgtPf+") and (lfc.lf_col_xref_id="+srcJoinColId+" or lfc.lf_col_xref_id="+tgtJoinColId+")";
				ResultSet rstableJoin=executeQuery(tableJoinAllQuery);
				while(rstableJoin.next()){
					TableJoinBean tableJoinBean=new TableJoinBean();
 					tableJoinBean.setFromTable(rstableJoin.getString("pf_name"));
					tableJoinBean.setColumnName(rstableJoin.getString("col_short_name"));
					String fromPfId=rstableJoin.getString("pf_id");
					Column[] colInLf=getColumnListInLf(rstableJoin.getString("lf_Id"));
					tableJoinBean.setColInLf(colInLf);
				    ResultSet rsFromPf=getDataSourcePF(fromPfId);
				    String fromUrl=getDataSourceUrlToReadData(rsFromPf);
				    tableJoinBean.setFromPf(fromUrl);
					tabJoin.add(tableJoinBean);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}
		else{
			String query="select * from operation o,logical_file l where o.from_lf=l.lf_id and  to_lf='"+padId+"'";
			ResultSet rs=executeQuery(query);
			try {
				while(rs.next()){
					
					TableJoinBean tableJoinBean=new TableJoinBean();
					String fromPfId=rs.getString("from_pf");
				    ResultSet rsFromPf=getDataSourcePF(fromPfId);
				    String fromUrl=getDataSourceUrlToReadData(rsFromPf);
				    tableJoinBean.setFromPf(fromUrl);
				    rsFromPf.beforeFirst();
					Column[] colInLf=getColumnListInLf(rs.getString("lf_Id"));
					tableJoinBean.setColInLf(colInLf);
					String fromTable=getPfTable(rsFromPf);
					tableJoinBean.setFromTable(fromTable);
					System.out.println(fromTable);
					tabJoin.add(tableJoinBean);
					

				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return tabJoin;
	}
	
	public Set<String> getPadFieldList(String padId){
		String query="select * from lf_col_xref l,column_dtl cd where l.col_id=cd.col_id and l.lf_col_xref_id in (select l.col_id from operation o,lf_col_xref l where o.to_lf=l.lf_id and o.to_lf='"+padId+"')";
		ResultSet rs=executeQuery(query);
		Set<String> padList=new HashSet<String>();
		try {
			while(rs.next()){
 			padList.add(rs.getString("col_short_name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return padList;
	}
	
	public CalcColumnBean getCalculationColumn(String calcId){
		CalcColumnBean clac=new CalcColumnBean();
		String query="select * from Calculation c ,column_dtl cd  where (c.date_field=cd.col_id or c.calculated_on=cd.col_id) and c.calc_id='"+calcId+"'";
		ResultSet rs=executeQuery(query);
		try {
			while(rs.next()){
				if(rs.getString("col_data_type").equals("Date"))
				clac.setDateCol(rs.getString("col_short_name"));
				else
				clac.setAmtCol(rs.getString("col_short_name"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return clac;
	}
	private void setPadCountInOperationTable(long padCount, String padId) {
		
				String query="update  operation o set o.number_of_keys='"+padCount+"' ,o.is_avilable=1  where o.to_lf='"+padId+"'";
		         int count=executeUpdate(query);
		         if(count>0)
		        	 System.out.println("Pad "+padId+" generated successfully");
		
	}
	public String getTableNameFromLogicalFile(String lfId){
		String table="";
		String query="select lf_name from logical_file where lf_id='"+lfId+"'";
		ResultSet rs=executeQuery(query);
		try {
			while(rs.next()){
				table=rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return table;
	}
	
	public String getFieldsInLogicalFile(String lfId){
		String table="";
		String query="select lf_name from logical_file where lf_id='"+lfId+"'";
		ResultSet rs=executeQuery(query);
		try {
			while(rs.next()){
				table=rs.getString(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return table;
	}
	public String getOtherLogicalFileId(String lfId){
		String table="";
		String query="select lf_id  from lf_col_xref where lf_col_xref_id ="
              +"(select tgt_lf_col_xref_id from column_join where src_lf_col_xref_if in(select lf_col_xref_id from lf_col_xref where lf_id='"+lfId+"'))";
		ResultSet rs=executeQuery(query);
		try {
			while(rs.next()){
				table=rs.getString(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return table;
	}
	public List<String> getPadField(String padId){
		List<String> createPadList=new ArrayList<String>();
		createPadList.add("cust_local_num");
		createPadList.add("cust_suff_num");
		createPadList.add("cust_local_src_sys_cd");
		return createPadList;
	}
	public ResultSet getDataSourcePF(String pfId){
		String query="select * from pf_ds_xref p,datasource d,dburl u,physical_file pf where p.ds_id=d.ds_id and  d.db_url_id=u.db_url_id "
		  		+ "and p.pf_id=pf.pf_id and p.Pf_id='"+pfId+"'";
		ResultSet rs=executeQuery(query);
		return rs;
	}
	public Column[] getColumnListInLf(String lfId){
		String query="select col_short_name from lf_col_xref l,column_dtl cd where l.col_id=cd.col_id and l.lf_id='"+lfId+"' and l.ops_id='' ";
		 Column[] colInLf=null;
		 try {
		 ResultSet rs=executeQuery(query);
		 int rows=0;
		 int i=0;
	        if (rs.last()) {
	        	  rows = rs.getRow();
	         	  rs.beforeFirst();
	        	}
	        colInLf=new Column[rows];
			while(rs.next()){
				colInLf[i]=new Column(rs.getString(1));
				i++;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return colInLf;
	}
}
